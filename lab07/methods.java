/*

//DJ Edwards

//Lab 07

//10.25.18

pt.1 We need some basic kinds of words.  We need: 
Adjectives
Non-primary nouns appropriate for the subject of a sentence
Past-tense verbs, and 
Non-primary nouns appropriate for the object of the sentence.

pt.2 Create four methods that correspond to the four sentence components above.  
Make each method generate a random integer from 0-9, select a random adjective / subject / verb / object from a switch statement, 
and return the word as a string.

pt.3 Create a loop in your main method that, at each iteration, assembles the words as defined by the 4 methods described 
above in Phase 0 into a string and prints it.  
The string generated should have appropriate articles between the random words, to maintain correct grammar. 

pt.4 Create a method that generates the sentence above.  Lets treat that sentence as a thesis sentence for a paragraph. 
To support that thesis, create a second method that generates an action sentence and refers to the subject of the first sentence.  
How do you know what the subject was?  When you generate the first sentence, return the subject (a String) as an output. 
Then pass the subject into your second method, so that your second method can use the same subject or use the word “it” randomly. 
*/






import java.util.Random;//
public class methods{



//methods here

	//ADJECTIVES
	public static String adjective(){

		String adj;

		Random rand = new Random();

		int  n = rand.nextInt(9) + 1;//random int



		switch(n){//adjectives
			case 1: adj = "quick";
							break;

			case 2: adj = "sensational";
							break;

			case 3: adj = "ugly";
							break;

			case 4: adj = "skinny";
							break;

			case 5: adj = "smart";
							break;

			case 6: adj = "majestic";
							break;

			case 7: adj = "tech savy";
							break;

			case 8: adj = "crazy";
							break;

			case 9: adj = "tired";
							break;

			default: adj = "default";
							break;

		}

					return adj;
	}

//SUBJECT NOUNS

		public static String subjectNouns(){

		String sN;

		Random rand = new Random();

		int  n = rand.nextInt(9) + 1;//random int

		switch(n){//subject nouns
			case 1: sN = "Doggie";
							break;

			case 2: sN = "Cat";
							break;

			case 3: sN = "Monkey";
							break;

			case 4: sN = "Zombie";
							break;

			case 5: sN = "Mayor of Bethleham";
							break;

			case 6: sN = "janitor";
							break;

			case 7: sN = "Power Ranger";
							break;

			case 8: sN = "Zoo Keeper";
							break;

			case 9: sN = "Nurse";
							break;

			default: sN = "default";
							break;

		}

					return sN;
	}

//OBJECT NOUNS
		public static String objectNouns(){

		String oN;

		Random rand = new Random();

		int  n = rand.nextInt(9) + 1;//random int

		switch(n){
			case 1: oN = "Woman";
							break;

			case 2: oN = "NBA Player";
							break;

			case 3: oN = "Police Officer";
							break;

			case 4: oN = "Sound Cloud Rapper";
							break;

			case 5: oN = "Clout Chaser";
							break;

			case 6:  oN = "Nun";
							break;

			case 7: oN = "Construction Worker";
							break;

			case 8: oN = "Lego";
							break;

			case 9: oN = "Doctor";
							break;

			default: oN = "default";
							break;

		}

					return oN;
	}

//PAST TENSE VERBS
		public static String pastTenseVerbs(){

		String pTV;

		Random rand = new Random();

		int  n = rand.nextInt(9) + 1;

		switch(n){
			case 1: pTV= "walked";
							break;

			case 2: pTV = "beep booped";
							break;

			case 3: pTV = "pushed";
							break;

			case 4: pTV = "talked to";
							break;

			case 5: pTV = "skipped";
							break;

			case 6:  pTV = "jump kicked";
							break;

			case 7: pTV = "played";
							break;

			case 8: pTV = "out danced";
							break;

			case 9: pTV = "punched";
							break;

			default: pTV = "default";
							break;

		}

					return pTV;
	}

	public static void firstAction(String obj){


 		String object = subjectNouns();

 		System.out.println("The "+adjective()+" "+obj+" "+pastTenseVerbs()+" the "+objectNouns()+".");//pieced toegther all four emthod return vales into a sentence


 		
 }	



 		public static  void secondAction(String obj){

 		System.out.println("The "+obj+" "+pastTenseVerbs()+" the "+objectNouns()+" shortly after.");//pieces second part of story, subject nouned is a string that is intialized in the main method



 	}

 		public static void conclusion(String obj){

 		System.out.println("and "+obj+" walked home and remembered the "+objectNouns()+" "+pastTenseVerbs()+".");//same as part 2


 	}


 	public static void master(){//calls all three methods to create the entite story. it also generates a random int that is the number of stories created

 	String sN;

 	Random rand = new Random();



	int  z = rand.nextInt(5) + 1;


	System.out.println(z+" story/stories created...");
	System.out.println("                                 ");

 for (int i = 0;i < z; i++){

 		int  n = rand.nextInt(9) + 1;

 	switch(n){
			case 1: sN = "Doggie";
							break;

			case 2: sN = "Cat";
							break;

			case 3: sN = "Monkey";
							break;

			case 4: sN = "Zombie";
							break;

			case 5: sN = "Mayor of Bethleham";
							break;

			case 6: sN = "janitor";
							break;

			case 7: sN = "Power Ranger";
							break;

			case 8: sN = "Zoo Keeper";
							break;

			case 9: sN = "Nurse";
							break;

			default: sN = "default";
							break;

		}

 	firstAction(sN);

	secondAction(sN);

	conclusion(sN);

	System.out.println("-----------------------------------");



 }



 	}




		

		
	











 public static void main(String[] args){//main method(runs the master method)


 	master();



 }


}