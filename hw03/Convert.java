// Hurricanes drop a tremendous amount of water on areas of land.  
// Write a program that asks the user for doubles that represent the number of 
//acres of land affected by hurricane precipitation and how many inches of rain were dropped on average.
//Convert the quantity of rain into cubic miles. Save the program as Convert.java


//Note that you can check your calculation using online calculators 
//that compute affected acres times inches of rain and generate gallons, then 
//you can use other online calculators to convert gallons to cubic miles.


//Dj Edwards
//CSE2
//Carr
import java.util.Scanner;//importing scanner

public class Convert{

	public static void main(String[] args) {



	Scanner scannerAcres = new Scanner(System.in);

      System.out.print("Enter the affected area in acres:");

      double acres = scannerAcres.nextDouble();//acres of area




      Scanner scannerRain = new Scanner(System.in);

      System.out.print("Enter the avg. inches of rainfall in the affected area:");

      double rainfall = scannerRain.nextDouble();//avg rain fall






   	  double cubicInches = (acres * rainfall);//gives the cubic inches of the entire area



   	  double gallons = cubicInches*(1.0/231.0);//converts cubic inches into gallons(231 cubic inches in 1 gallon)



	  double cubicFeet = gallons/7.48052;//converts gallons into cubic feet



      double cubicMiles = cubicFeet*.000000679357;//converts cubic feet into cubic miles


      System.out.println("cubic miles of rain is: "+cubicMiles);//result




	}

}