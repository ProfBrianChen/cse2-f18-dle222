//Write a program that prompts the user for the dimensions of a pyramid and 
//returns the volume inside the pyramid. Save the program in the file Pyramid.java. 
//The measurements should be of type double.


//Dj Edwards
//CSE2
//Carr

import java.util.Scanner;//importing scanner

public class Pyramid{
  


	public static void main(String[] args) {


		Scanner scannerBase = new Scanner(System.in);

		System.out.print("The square side of the pyramid is (input length):");

		double oneSide = scannerBase.nextDouble();

		double base = oneSide * oneSide;//overall area of base



		Scanner scannerHeight = new Scanner(System.in);

		System.out.print("The height of the pyramid is (input height):");

		double height = scannerHeight.nextDouble();//height of pyramid




		double result = (base * height)/3; //formula to find volume of pyramid.

		System.out.print("The volume inside the pyramid is: "+result);//result
  }
  
}