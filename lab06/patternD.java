//DJ Edwards
//12.18.18
//Carr
//PatternD
import java.util.Scanner;

public class patternD{


	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter a number for rows.");

		while(!myScanner.hasNextInt()){

			System.out.println("invalid input. Enter a number for rows.");//insures that user inputs an int

			myScanner.next();

		}

		int n = myScanner.nextInt();

		while(n<0 || n>10){//insures user input is greater than 0 and less than 10.

		System.out.println("invalid input. Enter a number for rows.");

		myScanner.next();
		}

for(int i = n; i >= 1; i--) //number of row
{
  for(int j = i; j >= 1; j--){ //controls output

    System.out.print(j + " "); //prints out number of columns
}
    System.out.println(); //prints a new line 
		}
  }
}
