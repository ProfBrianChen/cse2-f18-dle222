//DJ Edwards
//12.18.18
//Carr
//PatternB
import java.util.Scanner;

public class patternB{


	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter a number for rows.");

		while(!myScanner.hasNextInt()){//inusres that user inputs an int

			System.out.println("invalid input. Enter a number for rows.");

			myScanner.next();

		}

		int n = myScanner.nextInt();
		while(n<0 || n>10){//insures user input is greater than 0 and less than 10.

		System.out.println("invalid input. Enter a number for rows.");

		myScanner.next();
		}


		for (int i = n; i>0;i--){//controls rows(user input to 0)

			for(int k = 0; k<i;k++){//controls output(from 0 to being equal to k)

				System.out.print(k+1);//output
			}
			System.out.println();//prints new line

		}
	}

}