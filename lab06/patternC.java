//DJ Edwards
//12.18.18
//Carr
//PatternC
import java.util.Scanner;

public class patternC{


    public static void main(String[] args) {

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Enter a number for rows.");

        while(!myScanner.hasNextInt()){//insures that user inputs an int 

            System.out.println("invalid input. Enter a number for rows.");

            myScanner.next();

        }

        int n = myScanner.nextInt();
        while(n<0 || n>10){//insures user input is greater than 0 and less than 10.

        System.out.println("invalid input. Enter a number for rows.");

        myScanner.next();
        }

        for (int i = 1; i <= n; i++) { //rows

            for (int j = n; j > i; j--) { //controls spaces

                System.out.print(" ");
                }
        for (int k = i; k >= 1; k--){ //output

                System.out.print(k + "");

        }
                    System.out.println();//new line
    }


 
    }
}
