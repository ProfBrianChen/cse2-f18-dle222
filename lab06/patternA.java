//DJ Edwards
//12.18.18
//Carr
//PatternA
import java.util.Scanner;

public class patternA{

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter a number for rows.");

		while(!myScanner.hasNextInt()){//insures that user imputs an int

			System.out.println("invalid input. Enter a number for rows.");

			myScanner.next();

		}

		int n = myScanner.nextInt();
		while(n<0 || n>10){//insures user input is greater than 0 and less than 10.

		System.out.println("invalid input. Enter a number for rows.");

		myScanner.next();
		}

		n = myScanner.nextInt();

		for (int i = 0; i<n;i++){//controls number of the rows 

			for(int k = 0; k<i;k++){//controls number of columns.

				System.out.print(k+1);//prints out what column number loop is on.
			}
			System.out.println();//prints out next line

		}
	}

}