/*Program. 
1. Create a method called copy(), which accepts an integer array as input, and returns an integer array as output.  
It should declare and allocate a new integer array that is the same length as the input, use a for-loop to copy 
each member of the input to the same member in the new array.  Finally, it should return the new array as output.

2. Create a method, inverter(), which reverses the order of an array.  Inverter should work by accepting an array of 
integers as input.  Then it should systematically swap the first member of the input array with the last member, 
the second member of the input array with the second-to-last member, and so on.  Therefore it modifies the memory 
pointed to by the array input.  It returns void.

3. Create a second method, inverter2(), which first uses copy() to make a copy of the input array.  
Then it uses a copy of the code from inverter() to invert the members of the copy.  Finally, it returns the copy as output.

4. Create a method called print() which accepts any integer array as input, and returns nothing.  Use a for-loop to print all members of the array.
In your main method, declare a literal integer array of length at least 8 and make two copies of it using the copy() method, called array0, array1, array2.

5. Pass array0 to inverter() and print it out with print().  Do not assign the output of inverter to anything.

6. Pass array1 to inverter2() and print it out with print().  Do not assign the output of inverter to anything.

7. Pass array2 to inverter2() and assign the output to array3.  Array3 does not require an allocation, just declaration.  Print out array3 with print().

*/

//DJ Edwrads

//Carr

//11.16.18

//lab09

import java.util.Scanner;//importing scanner

import java.lang.String;

import java.util.Arrays;

public class lab09{


	public static int[] copy(int[] str){//copy method

		int[] copyy= new int[str.length];

		for (int i = 0; i < str.length; i++){

			copyy[i] = str[i];//copies each element in array
		}


		return copyy;



	}


	public static void inverter(int[] str){

		int beg = 0;
		int end = str.length-1;

		for (int i = 0; i<str.length/2; i++){



	int store = str[beg];//switches the beg value and end variables

	str[beg] = str[end];

	str[end] = store;


			beg++;//increments the variable coresponding to the start of array
			end--;//deincrements the variable coresponding to the end of array

		}

		System.out.println(Arrays.toString(str));



	}

		public static int[] inverter2(int[] str){

			inverter(copy(str));//invertes the copied array

			return str;
		
	}



	public static void print(int[] str){

		for(int i = 0; i<str.length; i++){

		System.out.print(str[i]);//prints each element in array with space between each

		System.out.print(" ");


	}
	System.out.println();

			

}


    public static void main(String[] args) {//main


    	int[] array0;

		array0 = new int[] {10,20,30,40,50,60,71,80,90,91};//universal array used

    	int[] array1 = copy(array0);

    	int[] array2 = copy(array0);

    	System.out.println();

    	System.out.println("ARRAY 0:");

    	// print(array0);

    	inverter2(array0);

    	print(array0);

    	System.out.println();

		System.out.println("ARRAY 1:");

		// print(array1);

    	inverter(array1);

    	print(array1);

    	System.out.println();

    	System.out.println("ARRAY 2:");

    	// print(array2);

    	inverter(array2);

    	print(array2);

    	System.out.println();

    	System.out.println("ARRAY 3:");

    	int[] array3 = inverter2(array2);

    	print(array3);





    	}


   }