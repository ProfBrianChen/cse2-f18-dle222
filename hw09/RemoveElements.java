
/*Program 2. I have provided the main program RemoveElements but I have omitted the code for three methods 
called in the main method, randomInput(), delete(list,pos), and remove(list,target). Write these methods. 
Below the program, I show the expected output. 

The randomInput() method generates an array of 10 random integers between 0 to 9.  Implement randomInput 
so that it fills the array with random integers and returns the filled array. 

The method delete(list,pos) takes, as input, an integer array called list and an integer called pos.  
It should create a new array that has one member fewer than list, and be composed of all of the same 
members except the member in the position pos.

Method remove(list,target) deletes all the elements that are equal to target, returning a new list without all those new elements.
*/

import java.util.*;



import java.util.Arrays;

import java.util.Scanner;

import java.util.Random;

import java.lang.Object;

import java.util.ArrayList;



public class RemoveElements{


  public static void main(String [] arg){

	 Scanner scan = new Scanner(System.in);

int num[]=new int[10];

int newArray1[];

int newArray2[];

int index,target;

	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();

  	try {
    newArray1 = delete(num,index);

    String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
  	continue;
}
catch(ArrayIndexOutOfBoundsException exception) {
  	System.out.print(" index not valid.");
    
}

 
    System.out.print("Enter the target value ");
  	target = scan.nextInt();
    try{

  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	continue;
  	}
  	catch(ArrayIndexOutOfBoundsException exception) {
  	System.out.print(" index not valid.");
    
}
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }

  public static int[] randomInput(){//creates random array of ints

  	Random rand = new Random();

  	 int randomArray[] = new int[10];//intializing

  	 

  	 for (int i = 0; i<10; i++){

  	 	int n = rand.nextInt(10);

  	 	randomArray[i] = n;

  	 }


  	 System.out.println(Arrays.toString(randomArray));

  	 return randomArray;

  }

    public static int[] delete(int list[],int pos){//delete

    	ArrayList<Integer> num = new ArrayList<Integer>();//creates arraylist that will later become an array

    	 // int num[]=new int[list.length];

    for(int i = 0; i<list.length; i++){//if the value in i index doesnt equal list at the given postion add it to the new array
        if(list[i] != list[pos]){
            num.add(list[i]);

        }


       }
      int[] array = new int[num.size()];//converts arraylist to array
		for (int i = 0; i < num.size(); i++) {
    array[i] = num.get(i);
		}



       
        // System.out.println(Arrays.toString(num));         

        return array;

       }

   	public static int[] remove(int list[],int target){//remove

    
  		
        ArrayList<Integer> num = new ArrayList<Integer>();//creates arraylist that will later become an array

    for(int i = 0; i<list.length; i++){

        if(list[i] != target){//uf the value at index i doesnt equal target add it 

            num.add(list[i]);
        }
    }

    // System.out.println(Arrays.toString(num));

       int[] array = new int[num.size()];
		for (int i = 0; i < num.size(); i++) {
    	array[i] = num.get(i);
		}

    return array;
               
        }



            

 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }

}




