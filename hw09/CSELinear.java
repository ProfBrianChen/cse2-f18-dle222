/*Program 1: Write a program called CSE2Linear.java that prompts the user to enter 15 ints for students’ 
final grades in CSE2. Check that the user only enters ints, and print an error message if the user enters 
anything other than an int.  Print a different error message for an int that is out of the range from 0-100, 
and finally a third error message if the int is not greater than or equal to the last int.  Print the final 
input array.  Next, prompt the user to enter a grade to be searched for. Use binary search to find the entered 
grade. Indicate if the grade was found or not, and print out the number of iterations used. 
Next, scramble the sorted array randomly, and print out the scrambled array. Prompt the user again to enter an
int to be searched for, and use linear search to find the grade. Indicate if the grade was found or not, and how many iterations it took.
Write separate methods for linear search, binary search and random scrambling. It might also help to write a print
method to track the progress of your array. Import and use java.util.Random for the random scrambling method.*/

//CHECKING TO SEE WHETHER IT IS GREATER THAN PREVIOUS ENTRY DOESNT WORK EVERYTHING ELSE DOES...


//Dj Edwards


import java.util.Scanner;//importing scanner

import java.util.Random;

import java.util.*;

public class CSELinear{

public static int times = 0;

public static int times2 = 0;

public static int grade[];



 public static int search(int arr[], int l, int r, int x){


    { 
        if (r>=l) //binary search 
        { 
            int mid = l + (r - l)/2;
            	hw092.times +=1;
  
            // If the element is present at the  
            // middle itself 
            if (arr[mid] == x) 
               return mid;
           		

  
            // If element is smaller than mid, then  
            // it can only be present in left subarray 
            if (arr[mid] > x) 
               return search(arr, l, mid-1, x);
           		
  
            // Else the element can only be present 
            // in right subarray 
            return search(arr, mid+1, r, x);
            
        } 
  
        // We reach here when element is not present 
        //  in array 
        return -1; 
    }
}

	public static int[] scramble(int arr[]){//scrambles array

		

		Random rand = new Random();

		

		for (int i = arr.length-1; i>0 ;i--){

		int n = rand.nextInt(i+1);

		int a = arr[n];
      	arr[n] = arr[i];//generates random int and puts it into the array at index i
      	arr[i] = a;

		}

		System.out.println(Arrays.toString(arr));

		return arr;


	}


	static int search2(int arr[], int n2, int x2) //binary
    { 
        for (int i = 0; i < n2; i++)
        {
        	times2 +=1;
   
            if (arr[i] == x2)         // Return the index of the element if the element 
            						 // is found 
                return i;

        } 
   
        // return -1 if the element is not found 
        return -1; 
    }


	public static void main(String[] args) {//main


		 int grades[] = new int[5]; 


		 Scanner myScanner = new Scanner(System.in);


		for(int i  = 0; i<5; i++){

        System.out.println("Please enter grade");

        // grades[i] = myScanner.nextInt();


         while (!myScanner.hasNextInt()) {
        System.out.println("Not a valid entry, please enter a number!");
        myScanner.next(); // this is important!
    }

     int temp = myScanner.nextInt();

//a third error message if the int is not greater than or equal to the last int
     	while (i != 0){
    	 while ((temp <= grades[i-1])) {//WHY THIS NO WORK!!!!!!!


        System.out.println("previous int is not less and or equal to int entered");
        temp = myScanner.nextInt(); // this is important!
   
}
}

     	 while ((temp>100)||(temp<0)){
        System.out.println("Not a valid entry, please enter a number between 1 and 100!");
        temp = myScanner.nextInt(); // this is important!
    }  

    grades[i] = temp;

   }

   System.out.println(Arrays.toString(grades));


   System.out.println("enter grade to search for");

    int x = myScanner.nextInt();

    int n = grades.length;



     int result = search(grades, 0, n-1, x);

      if (result == -1)

            System.out.println("Element not present"); 
        else
           	System.out.println("Element found at index " + result);

        System.out.println("it took " + hw092.times + " iterations");

        System.out.println("grades randomly scrambled like eggs...");

        scramble(grades);

        System.out.println("enter grade to search for");

    	int x2 = myScanner.nextInt();

    	int n2 = grades.length;

     	int result2 = search2(grades,n2-1, x2);//linaer seach here

     	if (result2 == -1)

            System.out.println("Element not present"); 
        else
           	System.out.println("Element found at index " + result2);

           System.out.println("it took " + hw092.times2 + " iterations");


		}
}