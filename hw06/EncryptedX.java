//Dj Edwards
//Hw06
//10.19.2018
//Carr

/*desription:  Ask the user for an integer between 0 - 100.
 Validate input. This will be the size of the square measured 
 by the number of stars. The screenshot above is a size 10 square, a 
 10x10 grid of characters. 
*/


import java.util.Scanner;//importing scanner
public class EncryptedX{

 public static void main(String[] args){//main method

 	int squareSize;

 	int firstSpacer=0;//first spacer is a value that is incremented every loop. It starts at the first value

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Please enter size of square");

        squareSize = myScanner.nextInt();

       if (squareSize<0||squareSize>100){//making sure sqaureSize is >0 and <100.

        System.out.println("Not a valid entry, please enter a number greater than 0 and less than 100!");

        myScanner.next(); // validation

		}

int secondSpacer=squareSize-1;//second spacer is a value that is deincremented every loop. It starts at the last value

for( int x = 0; x < squareSize; x++){//colums


for( int y = 0; y < squareSize; y++){//rows


if (firstSpacer == secondSpacer && y == x) {//if the first and last value are equal and the current row value and the current column value are equal, there must be a space there

	System.out.print(" ");


}else if (firstSpacer == y || secondSpacer == y)//if the last value and the current row value are equal, there should be a space there

System.out.print(' '); 

else

System.out.print('*'); //anywhere else needs a star

}
	firstSpacer+=1;//increasing the value secondSpacer by 1

	secondSpacer-=1;//decreasing the value secondSpacer by 1

System.out.println("");
}
}
}

